﻿using System;
using Xunit;

namespace PrivateTracer.CaregiversPortal.Test
{
    public class NormDateValidation
    {
        [Fact]
        public void IsValidNormDate()
        {
            DateTime date = DateTime.UtcNow.AddDays(-2);
            string dateString = date.ToString("dd-MM-yyyy");
            Assert.True(InputValidation.IsValidNormDate(dateString));
        }

        [Fact]
        public void IsValidNormDateInvalidString()
        {
            DateTime date = DateTime.UtcNow.AddDays(-2);
            string dateString = date.ToString("ddMMyyyy");
            Assert.False(InputValidation.IsValidNormDate(dateString));
        }

        [Fact]
        public void IsValidNormDateTimespan()
        {
            DateTime date = DateTime.UtcNow.AddDays(-30);
            string dateString = date.ToString("dd-MM-yyyy");
            Assert.False(InputValidation.IsValidNormDate(dateString));
        }

        [Fact]
        public void IsValidNormDateFuture()
        {
            DateTime date = DateTime.UtcNow.AddDays(1);
            string dateString = date.ToString("dd-MM-yyyy");
            Assert.False(InputValidation.IsValidNormDate(dateString));
        }
    }
}
