﻿using System;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PrivateTracer.CaregiversPortal.Pages
{
    [BindProperties]
    public class IndexModel : PageModel
    {
        private readonly IHttpClientFactory _clientFactory;
        public bool ClientCertIsValid;
        public string ClientCertSubject = "";
        public string AuthCode { get; set; }
        public string NormDate { get; set; }
        public string ButtonName { get; set; }
        public State CurrentState { get; set; }
        public string StatusMessage { get; set; }

        public enum State
        {
            input,
            success,
            error
        }

        public IndexModel(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public void OnGet()
        {
            SetBeginState();

            // All request without the X-ARR-ClientCert header will be bounced by the frontend load balancer.
            string clientCert = HttpContext.Request.Headers["X-ARR-ClientCert"];
            ClientCertIsValid = VerifyClientCertificate(clientCert);
        }

        public async Task OnPostAsync()
        {
            switch (ButtonName)
            {
                case "send":
                    if (!InputValidation.IsValidAuthCode(AuthCode) ||
                        !InputValidation.IsValidNormDate(NormDate))
                    {
                        CurrentState = State.error;
                        StatusMessage = "controleer de invoer.";
                        break;
                    }

                    string content = $"{{\"Token\": \"{ AuthCode }\"}}";
                    HttpClient httpClient = _clientFactory.CreateClient();
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://authorisationgatewaydev.azurewebsites.net/authorise")
                    {
                        Content = new StringContent(content, System.Text.Encoding.UTF8, "application/json")
                    };

                    var response = await httpClient.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        CurrentState = State.success;
                        StatusMessage = $"autorisatiecode { AuthCode } en vanaf datum { NormDate } verzonden.";
                        ClearInput();
                    }
                    else
                    {
                        CurrentState = State.error;
                        StatusMessage = $"server error ({ response.ReasonPhrase }).";
                    }
                    break;
                case "cancel":
                    ClearInput();
                    SetBeginState();
                    break;
                default:
                    break;
            }
        }

        private void SetBeginState()
        {
            CurrentState = State.input;
            StatusMessage = "verbonden.";
        }

        private void ClearInput()
        {
            AuthCode = null;
            NormDate = null;
        }

        private bool VerifyClientCertificate(string clientCertString)
        {
            if (string.IsNullOrEmpty(clientCertString)) return false;

            string validSubject = "";
            string validIssuerCN = "";
            string validIssuerO = "";
            string validThumbprint = "";

            try
            {
                byte[] clientCertBytes = Convert.FromBase64String(clientCertString);
                X509Certificate2 clientCert = new X509Certificate2(clientCertBytes);

                //Time validity
                if (DateTime.Compare(DateTime.Now, clientCert.NotBefore) < 0 || DateTime.Compare(DateTime.Now, clientCert.NotAfter) > 0) return false;


                //Subject
                ClientCertSubject = clientCert.Subject.Replace("CN=", "");
                bool foundSubject = false;
                string[] certSubjectData = clientCert.Subject.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in certSubjectData)
                {
                    if (String.Compare(s.Trim(), validSubject) == 0)
                    {
                        foundSubject = true;
                        break;
                    }
                }
                if (!foundSubject) return false;

                //Issuer
                bool foundIssuerCN = false, foundIssuerO = false;
                string[] certIssuerData = clientCert.Issuer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in certIssuerData)
                {
                    if (String.Compare(s.Trim(), validIssuerCN) == 0)
                    {
                        foundIssuerCN = true;
                        if (foundIssuerO) break;
                    }

                    if (String.Compare(s.Trim(), validIssuerO) == 0)
                    {
                        foundIssuerO = true;
                        if (foundIssuerCN) break;
                    }
                }
                if (!foundIssuerCN || !foundIssuerO) return false;

                //Thumprint
                if (String.Compare(clientCert.Thumbprint.Trim().ToUpper(), validThumbprint) != 0) return false;

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
