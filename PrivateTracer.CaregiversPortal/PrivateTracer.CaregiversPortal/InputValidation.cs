﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PrivateTracer.CaregiversPortal
{
    public static class InputValidation
    {
        private static readonly string validAuthCodeChars = "ABCDEFGHJKLMNPRSTUVWXYZ23456789";

        public static bool IsValidAuthCode(string authCode)
        {
            if (authCode == null) { return false; }

            string authCodeWithoutDashes = authCode.Replace("-", "");
            if (authCodeWithoutDashes.Length != 10) { return false; }

            if (!Regex.IsMatch(authCodeWithoutDashes, $@"[{ validAuthCodeChars }]")) { return false; }

            if (!IsValidLuhnChecksum(authCodeWithoutDashes)) { return false; }

            return true;
        }

        public static bool IsValidLuhnChecksum(string authCodeWithoutDashes)
        {
            // From Android client implementation
            int factor = 1;
            int sum = 0;
            int n = validAuthCodeChars.Length;

            // Starting from the right, work leftwards
            // Now, the initial "factor" will always be "1"
            // since the last character is the check character.
            for (int i = authCodeWithoutDashes.Length - 1; i >= 0; i--)
            {
                int codePoint = validAuthCodeChars.IndexOf(authCodeWithoutDashes[i]);
                int addend = factor * codePoint;
                // Alternate the "factor" that each "codePoint" is multiplied by
                if (factor == 2) { factor = 1; } else { factor = 2; }
                // Sum the digits of the "addend" as expressed in base "n"
                addend = addend / n + addend % n;
                sum += addend;
            }

            int remainder = sum % n;
            return remainder == 0;
        }

        public static bool IsValidNormDate(string normDate)
        {
            DateTime.TryParseExact(normDate, "dd-MM-yyyy", new CultureInfo("nl-NL"), DateTimeStyles.AdjustToUniversal, out DateTime date);

            if (date == null) { return false; }
            
            if (date < DateTime.UtcNow.AddDays(-21)) { return false; }

            if (date > DateTime.UtcNow) { return false; }

            return true;
        }
    }
}
