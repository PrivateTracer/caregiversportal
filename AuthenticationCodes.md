# Authentication Codes

For the doctor's (caregiver's) approval of positive test, a test authentication code (*in short:* AuthCode) is used, as described [here](https://gitlab.com/PrivateTracer/coordination/-/blob/master/usecases/TrustedInfectionStatus.md).
This document describes the intended process for the Authentication Code use throughout the Doctor's Approval flow.

## Properties
Adhering to PrivateTracer's privacy-by-design principles, the following properties apply for the AuthCode end-to-end flow:

 1. It is generated on the patient's device
 2. It is generated **randomly** using the platforms secure random generator (https://developer.apple.com/documentation/security/randomization_services, android secure random number generator)
 3. Doctor and patient should be able to easily **exchange** the code **by phone**.
 4. No central server or authority will check or confirm **uniqueness**
 5. A **check digit** is added to support the doctor in validating their input
 
NB. further details on the bold phrases follow below.

## Randomness
The AuthCode must be generated in such a way that they are 
- unpredictable from previously generated codes
- not traceble to the device
- do we need forward secrecy?

 
## Exchange by phone
To allow exchange by phone, the AuthCode:

 - consists of letters and numbers
 - consists of only capitals (to prevent confusion on B vs b, or L vs. l)
 - does not contain any O,Q,0,I or 1 (to prevent confusion).

The remaining character sets consists of 23 letters and 8 numbers.

## Uniqueness / entropy
To prevent to risk that clients can check which AuthCodes are already submitted to the MobileClient queue, no confirmation is provided to the client that a submitted AuthCode exists or not. To minimize the risk that non-unique AuthCodes are submitted, the entropy must be sufficiently high.

Assumed is a key lifetime of 10 days: time between AuthCode creation and Doctor's approval and assuming 100k tests per day, the number of active AuthCodes is ~1 Mio.

With an AuthCode length of 10 characters minus a checksum digit, the risk of a duplicate is 1/(31^9 / 10^6) = ~1/26e6.

Note that potential duplicates should be prevented, but if they occur, they are not a critical problem. They may grow the seed file, and only a tiny fraction of that might result in false positives.

## Check digit
The check digit is used in the front end to provide the doctor with feedback in case of a typo. 

Two situations may occur:
- the doctor could mistype the code from his records, in this case, the check digit supports him to correct immediately.
- the doctor may have an error in his records. The check digit will notify the doctor, who then needs to retrieve the correct code from the patient.

If no AuthCode can be saved on the patient's device, the check digit will only solve situation 1, in situation 2 the patient's trace would be lost.


For now, we choose the Luhn mod N algorithm, as described on https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm.
The advantages are that it (1) neatly handles both numbers and letters, (2) results in check digits within the selected character subset. 
It does only capture single digit errors, no transpositions.

The following character mapping is used: 

`VALID_CHARS = "ABCDEFGHJKLMNPRSTUVWXYZ23456789"`

`codePoint(character) = VALID_CHARS.IndexOf(character)`

| character  | A | B | C | D | E | F | G | H | J | K | L  | M  | N  | P  | R  | S  | T  | U  | V  | W  | X  | Y  | Z  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  |
|------------|---|---|---|---|---|---|---|---|---|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
| codePoint | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 |

Alternative implementations under ISO 7064 (IBAN, BIC, a.o.) are able to capture transpositions, but require implementation-specific changes to comply with the other requirements. 
For instance, for consistency with phone communication, check digits of 0 and 1 must be replaced by allowed characters.

### Input format checks front-end
- AuthCode = length 10
- AuthCode only contains valid characters A-H,J-N,P,R-Z,2-9.
- The check digit must be valid
- Format of NormDate is local time (CET/CEST), submitted as UTC to the server.

### Server response codes
- if date format is not UTC: 400
- if AuthCode does not match valid character set: Bad format 400
- No matching AuthCode in queue: 404. (This enables scanning for valid AuthCodes, but only for autorised caretakers.This message is only useful if patient's device will store AuthCode, see Extensions)
- Cannot convert NormDate to Epoch > pandemic start = 404 Pandemic not found
- If all NormDates after ALL AuthCode Epochs i.e. nothing to go in the TracingSet: STILL a 200?

It is assumed that the Server is protected by something such as the "de Nationale Wasstraat tegen DDoS-aanvallen"https://www.nbip.nl/nawas/ - allowing it to priortise/gate on National/dutch ISP/mobile-peering point uploads. Thus making any issues with stuffing managable (on the times scales of days and 100k-valid submmissions/day)

## Extensions / alternatives 

### Storing the AuthCode on the patient's device.

By storing the AuthCode on the patient's device, the doctor can retrieve it in case it goes missing or misspelled.
It must be prevented that it could be considered as an indicator that the patient has been tested or is tested positive. 
Therefore, with this extension, the following principles are added:

6. The AuthCode will be stored on the patient's device, to allow the doctor to retrieve it in case the code is lost or corrupted.
7. The app should *always* show a code with valid check digit on every device, regardless of test, upload and/or seed status. 

Only after a patient has generated an AuthCode, the actual seed upload takes place. The AuthCode should remain constant voor the lifetime of 10 days, or until a new AuthCode is requested by the patient.

The risk with having this submission-related auth-code saved on the patients phone **and visible post/outside the medical interaction process** is that of things scuh as coercion; the bearer may be required (e.g. by an employer) to prove that a test was taken.

## UX issues / questions

 - How do we prevent patients from uploading many AuthCodes from the app?
 -- dirkx: propably not a concern; they would never be let through to the infected list; and the impact on the process is neglible.
 - since vowels A,E,U are included in the AuthCode character set, words might be formed coincidentally. Can UI/UX ensure that patients will not be tempted to read words to the doctor (risking misinterpretation)?
