# Care givers portal

In PrivateTracer, only positive test results are submitted, and only with consent of the patient. 

The patient consents after consultation with their GP, by generating a unique, anonymous code with the PrivateTracer app. The GP preserves this code in his patient's file. After obtaining a positive test result, the doctor will submit the patient's code. 

The patient preserves the right, not to know the test result in this flow.

See also:  Juridische handreiking Gegevensuitwisseling in de infectieziektebestrijding, https://www.rivm.nl/documenten/juridische-handreiking

## authentication

Only category 3 care professionals in the Dutch UZI register (www.uziregister.nl) are allowed to submit authorisation codes for positive tests. This is verified by the doctor's UZI pass. 

On a computer with a UZI pass card reader installed, the doctor visits https://doctor.privatetracer.org/. 

It is assumed that virtually any Dutch doctor has access to such a computer, since it is used regularly for communication through the the Landelijk Schakelpunt (LSP). More info on UZI and software is available here: https://www.uziregister.nl/uzi-pas/aanvraagproces/gebruik-van-de-pas.

On opening the landing page, a pop-up screen from the card reader software, such as SafeSign Client, will request the doctor's authentication code. After this, the doctor can directly submit a patient's authorisation code.

## demo

In the demo version, for demo purposes, any (self-signed) certificate is accepted, and the UZI certificate is not validated.

### using a test certificate
Note that the website will return a 403 error if it doesn't receive a certificate from the browser. This has to be a UZI certificate, but for demo purposes this can be any (self-singed) certificate. You can create a self-singed certificate in, for example, 

**For windows users**

Powershell (admin): $cert = New-SelfSignedCertificate -certstorelocation cert:\currentuser\my -dnsname uzi.test

**For Mac users**

Create self-signed certificates in Keychain Access on Mac:
1. In the Keychain Access app on your Mac, choose Keychain Access > Certificate Assistant > Create a Certificate.
2. Enter a name for the certificate.
3. Choose an Identity type, then choose the type of certificate. For an explanation of certificate types, click Learn More. ...
4. Click Create.
5. Review the certificate, then click Done.

## Recap

So specifically the order of steps is:

*	The GP explains the options.
*	The GP gets informed consent from the patient.
*	The  generating a unique, anonymous `code` with the PrivateTracer app and shares this with the GP
* 	The GP reserves this `code` in his patient's file (note 1)
* 	The test is submitted
*	The test results are sent back to the GP a few hours/days later.
*	The GP evaluates the test results and, following professional guidelines, may decide that this requires disclosure
*	The GP uses a digital version of his or her medical credentials to log onto https://doctor.privatetracer.org/. 
*	The GP enters the `code` from the patients file
*	The system will schedule distribution of the related seeds to the relevant population; normally within the next 24 hours.
*	The GP contacts the patient as and when required to share this information & aftercare.
*	The GP updates the medical file as required.

Note 1: As an alternative flow, if positive test results are to always yield distribution, the GP may, rather than enter it into the patient file, include the `code` with the test submitted. And the test lab (of which there are only a few) submits the `code`s in bulk as part of their normal automated processes.

